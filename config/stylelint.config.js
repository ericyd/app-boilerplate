// basic config for stylelint.
// can be put into package.json, but this felt cleaner.
// extends is the config that it uses,
// then it lists custom override rules
module.exports = {
    extends: 'stylelint-config-standard',
    rules: {
        'indentation': 4,
        'length-zero-no-unit': null,
        'declaration-empty-line-before': null
    }
};
