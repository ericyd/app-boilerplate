/**
 * For more info on any of this, check out:
 * 1. https://github.com/verekia/js-stack-from-scratch/tree/master/tutorial/7-client-webpack
 * 2. create-react-app hello-world
 */

const paths = require('./paths');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const postcssImport = require('postcss-import');
const autoprefixer = require('autoprefixer');
const webpack = require('webpack');

// I'm really not sure of the purpose of this variable,
// but the production config file in create-react-app says
// it is very important that it is set this way, for use in the webpack.DefintePlugin() call below
// ... so I'm a sheep
const env = { 'process.env': { NODE_ENV: '"production"', PUBLIC_URL: '""' } };

module.exports = {
    // this is where webpack starts when bundling.
    // any code that needs to be included should either be listed here 
    // or be a dependency of code listed here.
    // Note: Alternative syntax for creating separate bundles:
    //      entry: {
    //          bundleOne: [
    //              file1,
    //              file2
    //          ],
    //          bundeTwo: [
    //              file1,
    //              file2
    //          ]
    //      }
    // you can then reference this in output
    //      output: {
    //          path: paths.appBuild,
    //          filename: '[name].js',
    //          publicPath: '/'
    //      },
    entry: [
        require.resolve('./polyfills'),
        paths.appIndexJs
    ],
    output: {
        path: paths.appBuild,
        filename: 'index.js',
        publicPath: '/'
    },
    // setting this to eval is better for dev, I guess
    devtool: 'source-map',
    // plugins... not totally sure on these yet
    plugins: [
        new StyleLintPlugin({
            configFile: './config/stylelint.config.js',
            syntax: 'scss',
            failOnError: false
        }),
        new webpack.DefinePlugin(env),
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                screw_ie8: true, // React doesn't support IE8
                warnings: false
            },
            mangle: {
                screw_ie8: true
            },
            output: {
                comments: false,
                screw_ie8: true
            }
        }),
        new ExtractTextPlugin('app.css')
    ],
    // properties that are the names of plugins or loaders typically
    // contain some sort of config information for that loader. 
    // In this case, we're just telling eslint where to look for the config
    // file (default is root or in package.json)
    eslint: {
        configFile: 'config/.eslintrc'
    },
    module: {
        preLoaders: [
            {
                test: /\.(js|jsx)$/,
                loader: 'eslint-loader',
                include: paths.appSrc
            }
        ],
        loaders: [
            {
                // loads all javascript
                test: /\.jsx?$/,
                loader: 'babel-loader',
                include: paths.appSrc,
                exclude: [/node_modules/]
            },
            {
                // loads sass and scss files
                test: /\.(sc|sa|c)ss$/,
                loader: ExtractTextPlugin.extract('style-loader', 'css-loader!sass-loader!postcss-loader' ) 
            },
            {
                test: /\.md$/,
                // this uses the html loader and applies the markdown loader to it
                // my understanding is that the ! essentially chains together functionality
                // gfm is "github flavored markdown"
                loader: 'html!markdown?gfm=false'
            }
        ]
    },
    resolve: {
        extensions: ['', '.js', '.jsx'],
        // this makes it so you can import modules like require('components')
        // instead of require('../../components');
        modulesDirectories: ['node_modules', './src']
    },
    // configure postcss
    // autoprefixer is nice for putting vendor prefixes on things
    // postcss-import is nice if not using sass
    postcss: [
        autoprefixer()
    ],
    // Some libraries import Node modules but don't use them in the browser.
    // Tell Webpack to provide empty mocks for them so importing them works.
    node: {
        fs: 'empty',
        net: 'empty',
        tls: 'empty'
    }
};
