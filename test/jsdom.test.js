'use strict';

import jsdom from 'mocha-jsdom';
import { expect } from 'chai';
 
describe('jsdom test', function () {
    // give extra time for jsdom to iniatialize
    this.timeout('4000');
    jsdom()
 
    it('has document', function () {
        var div = document.createElement('div')
        expect(div.nodeName).eql('DIV')
    })
})