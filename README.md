# Readme

Ideally, this will be populated with information
about the app that is built, not the boilerplate
used to jumpstart the app.

## Usage

### Requirements

1. [Git](https://git-scm.com/)
2. [Node, 6.x](https://nodejs.org/en/)

### Getting started

1) Clone repo
```
git clone https://bitbucket.org/ericyd/app-boilerplate.git my-app
```
2) Install dependencies
```
npm install
```
3) Build
```
npm run build
```
4) Run local server.  You can do this a few ways, but this example uses [pushstate-server](https://www.npmjs.com/package/pushstate-server)
```
npm install -g pushstate-server
pushstate-server build
```
5) Open browser, and navigate to [localhost:9000](http://localhost:9000)