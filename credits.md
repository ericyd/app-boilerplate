# Credits

This boilerplate relied heavily on 
(and lifted some serious code from) the following:

1. [create-react-app](https://www.npmjs.com/package/create-react-app)
2. [js-stack-from-scratch](https://github.com/verekia/js-stack-from-scratch)
3. of course, [stack overflow](http://stackoverflow.com/)
4. [ericyd](http://ericyd.com) - I refactored and wrote a bit 
   of code to bring the above resources together
5. Probably lots of others, I'll try to add more to this list 
   as I remember them